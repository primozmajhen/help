package net.app.help.model;

/**
 * Created by Boogaboo on 18.12.2014.
 */
public class Contact {

	private int id;
	private int id_grp;
	private int id_msg;
	private String name;
	private String tel;

	public Contact() {
		this.id = 0;
		this.id_grp = 0;
		this.id_msg = 0;
		this.name = "";
		this.tel = "";
	}

	@Override
	public String toString() {
		return "ID="+id+" NAME="+name+" TEL="+tel;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getId_grp() {
		return id_grp;
	}

	public void setId_grp(int id_grp) {
		this.id_grp = id_grp;
	}

	public int getId_msg() {
		return id_msg;
	}

	public void setId_msg(int id_msg) {
		this.id_msg = id_msg;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}
}
