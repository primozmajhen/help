package net.app.help.adapters;

import android.app.Activity;
import android.app.FragmentManager;
import android.content.Context;
import android.database.DataSetObserver;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ListAdapter;
import android.widget.TextView;

import net.app.help.R;
import net.app.help.dialogs.EditGroupDialog;
import net.app.help.model.Contact;
import net.app.help.model.Group;
import net.app.help.model.Message;

import java.util.ArrayList;

/**ListView, ki prikazuje skupine z Itemom, ki vsebuje title, začetek contenta ter CheckBox, s katerim
 * se skupine izberejo, da se lahko nato proži Remove Group (Dialog, ki samo omogoči potrditev),
 * AttachMessageFragment, ki prikaže vsa Message in omogoči, da se z RadioButtonom izbere sporočilo, ki
 * se s pritiskom Confirm pripne.
 * Ali RemoveMessageDialog, ki prikaže vse izbrane Groups z obkljukanim CheckBoxom in ostale, ki sicer
 * imajo pripeto sporočilo a z neobkljukanim CheckBoxom in ob pritisku Confirm omogoči odstranitev Message.
 * Created by Boogaboo on 6.1.2015.
 */
public class GroupsListViewAdapter extends BaseAdapter implements ListAdapter {

	private boolean[] selected;
	private ArrayList<Group> groups;
	private ArrayList<Message> messages;
	private ArrayList<Contact> contacts;
	private LayoutInflater inflater;
	private GroupHolder gh;
	private FragmentManager fm;

//	private AdapterView.OnItemLongClickListener editGroupListener;

	public GroupsListViewAdapter(ArrayList<Group> groups, ArrayList<Message> messages, ArrayList<Contact> contacts, Context context/*, AdapterView.OnItemLongClickListener editGroupListener*/) {
		selected = new boolean[groups.size()];
		inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.groups = groups;
		this.messages = messages;
		this.contacts = contacts;
		this.fm = ((Activity)context).getFragmentManager();
//		this.editGroupListener = editGroupListener;
	}


	@Override
	public boolean areAllItemsEnabled() {
		return false;
	}

	@Override
	public boolean isEnabled(int position) {
		return false;
	}

	@Override
	public void registerDataSetObserver(DataSetObserver observer) {

	}

	@Override
	public void unregisterDataSetObserver(DataSetObserver observer) {

	}

	@Override
	public int getCount() {
		return groups.size();
	}

	@Override
	public Object getItem(int position) {
		return groups.get(position);
	}

	@Override
	public long getItemId(int position) {
		return (long) groups.get(position).getId();
	}

	@Override
	public boolean hasStableIds() {
		return false;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		View row = convertView;

		if (convertView == null) {
			row = inflater.inflate(R.layout.list_view_groups, parent, false);
			gh = new GroupHolder();
			gh.name = (TextView) row.findViewById(R.id.group_tv_name);
			gh.message = (TextView) row.findViewById(R.id.group_tv_message);
			gh.memberNumber = (TextView) row.findViewById(R.id.group_tv_members);
			gh.cb = (CheckBox) row.findViewById(R.id.group_tv_checkbox);
			row.setTag(gh);
		} else {
			gh = (GroupHolder) row.getTag();
		}
		final Group g = groups.get(position);
		gh.name.setText(g.getName());
		for (int i=0; i<messages.size(); i++) {
			if (g.getId_msg() == messages.get(i).getId()) {
				gh.message.setText(messages.get(i).getTitle());
			}
		}
		int counter = 0;
		for (int i=0; i<contacts.size(); i++) {
			if (g.getId() == contacts.get(i).getId_grp()) {
				counter++;
			}
		}
		gh.memberNumber.setText(String.valueOf(counter));
		gh.cb.setChecked(selected[position]);
		gh.cb.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (((CheckBox)v).isChecked())
					selected[position] = true;
				else
					selected[position] = false;
			}
		});

		row.setOnLongClickListener(new View.OnLongClickListener() {
			@Override
			public boolean onLongClick(View v) {
				Log.d("onLongClick", "position="+position);
//				Group g = (Group) groupsListViewAdapter.getItem(position);
//				OPEN EditGroupDialog in pošlji not izbrano skupino, ter vse kontakte, prikaži le kontakte ki pripadajo skupini
				EditGroupDialog egd = EditGroupDialog.newInstance(g);
				egd.show(fm, "EditGroupDialog");
				return false;
			}
		});

		return row;
	}

	@Override
	public int getItemViewType(int position) {
		return 0;
	}

	@Override
	public int getViewTypeCount() {
		return 1;
	}

	@Override
	public boolean isEmpty() {
		return groups.isEmpty();
	}

	public boolean[] obtainSelected() {
		return selected;
	}

	private class GroupHolder {
		TextView name;
		TextView message;
		TextView memberNumber;
		CheckBox cb;
	}

}
