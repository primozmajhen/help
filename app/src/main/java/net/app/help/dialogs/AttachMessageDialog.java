package net.app.help.dialogs;

import android.app.Activity;
import android.app.DialogFragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import net.app.help.R;
import net.app.help.adapters.AttachMessageListViewAdapter;
import net.app.help.db.DatabaseAdapter;
import net.app.help.model.Contact;
import net.app.help.model.Group;
import net.app.help.model.Message;

import java.util.ArrayList;

/**
 * Created by Boogaboo on 11.1.2015.
 */
public class AttachMessageDialog extends DialogFragment {

	private ListView messagesList;
	private Button confirm;
	private AttachMessageListViewAdapter attachMessageListViewAdapter;

	private ArrayList<Message> messages;
	private ArrayList<Contact> selectedContacts;
	private ArrayList<Group> selectedGroups;
	private boolean[] selectedMessage;

	private DatabaseAdapter da;

	private onConfirmInterface callback;

	public static AttachMessageDialog newInstance(ArrayList<Message> messages, ArrayList<Contact> selectedContacts, ArrayList<Group> selectedGroups) {
		AttachMessageDialog amd = new AttachMessageDialog();
//		Log.d("AttachMessageDialog newInstance","selectedContacts.size="+selectedContacts.size()+" selectedGroups.size="+selectedGroups.size());
		amd.messages = messages;
		amd.selectedContacts = selectedContacts;
		amd.selectedGroups = selectedGroups;
		return amd;
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			callback = (onConfirmInterface) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString() + " must implement OnConfirmInterface.");
		}
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		Log.d("AttachMessageDialog onCreate",".....");
		super.onCreate(savedInstanceState);
	}

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.fragment_attach_message, container);

		da = new DatabaseAdapter(getActivity());
		messagesList = (ListView) v.findViewById(R.id.frag_att_msg_lv_message_list);
		messagesList.setChoiceMode(AbsListView.CHOICE_MODE_SINGLE);
		attachMessageListViewAdapter = new AttachMessageListViewAdapter(messages, getActivity());
		messagesList.setAdapter(attachMessageListViewAdapter);

		confirm = (Button) v.findViewById(R.id.frag_att_msg_b_attach_message);

		confirm.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
//				OBTAIN SELECTED MESSAGE AND ADD IT TO ALL SELECTED CONTACTS
				selectedMessage = attachMessageListViewAdapter.obtainSelected();
				Message m = new Message();
				for (int i=0; i<messages.size(); i++) {
					if (selectedMessage[i]) {
						m = messages.get(i);
					}
				}
				da.openToWrite();
				/*ČE SO BILI DODANI IZBRANI KONTAKTI DODAJ SPOROČILO KONTAKTOM*/
				if (selectedContacts.size() != 0) {
					for (int i = 0; i < selectedContacts.size(); i++) {
						Log.d("onCreateView", "Attaching message m=" + m.toString() + " to contact=" + selectedContacts.get(i).toString());
						da.addContactAMessage(selectedContacts.get(i), m);
					}
				}
				/*ČE SO BILE IZBRANE SKUPINE DODAJ SPOROČILO SKUPINAM*/
				if (selectedGroups.size() != 0) {
					for (int i = 0; i < selectedGroups.size(); i++) {
						Log.d("onCreateView", "Attaching message m=" + m.toString() + " to group=" + selectedGroups.get(i).toString());
						da.addGroupAMessage(selectedGroups.get(i), m);
					}
				}
				da.close();
				if (selectedContacts.size() > 0 || selectedGroups.size() > 0)
					callback.onConfirmAttachMessage();
				else
					callback.onCancelAttachMessage();
				dismiss();
			}
		});

		return v;
	}


	public interface onConfirmInterface {
		public void onConfirmAttachMessage();
		public void onCancelAttachMessage();
	}

}
