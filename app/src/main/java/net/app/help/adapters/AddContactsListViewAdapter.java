package net.app.help.adapters;

import android.content.Context;
import android.database.DataSetObserver;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ListAdapter;
import android.widget.TextView;

import net.app.help.R;
import net.app.help.model.Contact;

import java.util.ArrayList;

/**
 * Created by Boogaboo on 10.1.2015.
 */
public class AddContactsListViewAdapter extends BaseAdapter implements ListAdapter {

	private boolean[] checkBoxState;

	private ArrayList<Contact> contacts;
	private LayoutInflater inflater;

	public AddContactsListViewAdapter(ArrayList<Contact> contacts, Context context) {
		this.checkBoxState = new boolean[contacts.size()];
		this.contacts = contacts;
		inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public boolean areAllItemsEnabled() {
		return false;
	}

	@Override
	public boolean isEnabled(int position) {
		return false;
	}

	@Override
	public void registerDataSetObserver(DataSetObserver observer) {

	}

	@Override
	public void unregisterDataSetObserver(DataSetObserver observer) {

	}

	@Override
	public int getCount() {
		return contacts.size();
	}

	@Override
	public Object getItem(int position) {
		return contacts.get(position);
	}

	@Override
	public long getItemId(int position) {
		return (long) contacts.get(position).getId();
	}

	@Override
	public boolean hasStableIds() {
		return false;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		View row = convertView;
		ContactHolder ch = null;

		if (convertView == null) {
			row = inflater.inflate(R.layout.list_view_add_contact_dialog, parent, false);
			ch = new ContactHolder();
			ch.name = (TextView) row.findViewById(R.id.add_contact_dialog_tv_name);
			ch.tel = (TextView) row.findViewById(R.id.add_contact_dialog_tv_tel);
			ch.cb = (CheckBox) row.findViewById(R.id.add_contact_dialog_checkbox);
			ch.cb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
				@Override
				public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

				}
			});
			row.setTag(ch);
		} else {
			ch = (ContactHolder) row.getTag();
		}

		Contact c = contacts.get(position);
		ch.name.setText(c.getName());
		ch.tel.setText(c.getTel());
		ch.cb.setChecked(checkBoxState[position]);
		ch.cb.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (((CheckBox)v).isChecked()) {
					checkBoxState[position]=true;
				} else {
					checkBoxState[position]=false;
				}
			}
		});

		return row;
	}

	@Override
	public int getItemViewType(int position) {
		return 1;
	}

	@Override
	public int getViewTypeCount() {
		return 1;
	}

	@Override
	public boolean isEmpty() {
		return false;
	}

	public boolean[] obtainSelected() {
		String log = "";
		for (int i=0; i<checkBoxState.length; i++) {
			log += "checkBoxState["+i+"]="+checkBoxState[i]+" ; ";
		}
		Log.d("obtainSelected ContactsListViewAdapter", log);
		return checkBoxState;
	}

	private class ContactHolder {
		TextView name;
		TextView tel;
		CheckBox cb;
	}

}
