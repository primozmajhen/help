package net.app.help.dialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import net.app.help.R;
import net.app.help.adapters.EditGroupContactsListViewAdapter;
import net.app.help.db.DatabaseAdapter;
import net.app.help.model.Contact;
import net.app.help.model.Group;
import net.app.help.model.Message;

import java.util.ArrayList;

/**
 * Created by Boogaboo on 12.1.2015.
 */
public class EditGroupDialog extends DialogFragment {

	private EditText name;
	private Button confirm;

	private Group selectedGroup;

	private DatabaseAdapter da;

	private OnConfirmInterface callback;


	public static EditGroupDialog newInstance(Group g) {
		EditGroupDialog egd = new EditGroupDialog();
		egd.selectedGroup = g;
		return egd;
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			callback = (OnConfirmInterface) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString() + " must implement OnConfirmInterface.");
		}
	}

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.fragment_edit_group, null);

		da = new DatabaseAdapter(getActivity());
		name = (EditText) v.findViewById(R.id.frag_edit_grp_et_name);
		name.setText(String.valueOf(selectedGroup.getName()));
		confirm = (Button) v.findViewById(R.id.frag_edit_grp_b_confirm);
		confirm.setOnClickListener(confirmListener);

		return v;
	}

	private View.OnClickListener confirmListener = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
//			IMPLEMENTIRAJ SHRANJEVANJE NOVEGA STANJA - NAME,...?
			if (!name.getText().toString().equals(selectedGroup.getName())) {
				selectedGroup.setName(name.getText().toString());
				da.openToWrite();
				da.updateGroup(selectedGroup);
				da.close();
			}
			callback.onConfirmEditGroup();
			dismiss();
		}
	};

	public interface OnConfirmInterface {
		public void onConfirmEditGroup();
		public void onCancelEditGroup();
	}

}
