package net.app.help.adapters;

import android.content.Context;
import android.database.DataSetObserver;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;

import net.app.help.R;
import net.app.help.model.Contact;
import net.app.help.model.Group;
import net.app.help.model.Message;

import java.util.ArrayList;

/**ListView, ki prikazuje vse Contacte iz DB in ima CheckBox, da se jih lahko več hkrati izbriše, ali
 * da se lahko večim hkrati pripne sporočilo. Enako, da se lahko odpre RemoveMessageDialog, ki
 * prikaže vse izbrane kontakte + kontakte, ki imajo pripeto sporočilo a so neizbrani. Vsi ti kontakti
 * imajo torej v ListView itemu CheckBox, brisanje Message pa se potrdi s pritiskom na Confirm.
 * Created by Boogaboo on 6.1.2015.
 */
public class ContactsListViewAdapter extends BaseAdapter implements ListAdapter {

	private boolean[] checkBoxState;
	private ArrayList<Contact> contacts;
	private ArrayList<Group> groups;
	private ArrayList<Message> messages;
	private Group selectedGroup;
	private LayoutInflater inflater;

	public ContactsListViewAdapter(ArrayList<Contact> contacts, ArrayList<Group> groups, ArrayList<Message> messages, Context context) {
		this.checkBoxState = new boolean[contacts.size()];
		this.contacts = contacts;
		this.groups = groups;
		this.messages = messages;
		inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public boolean areAllItemsEnabled() {
		return false;
	}

	@Override
	public boolean isEnabled(int position) {
		return false;
	}

	@Override
	public void registerDataSetObserver(DataSetObserver observer) {

	}

	@Override
	public void unregisterDataSetObserver(DataSetObserver observer) {

	}

	@Override
	public int getCount() {
		return contacts.size();
	}

	@Override
	public Object getItem(int position) {
		return contacts.get(position);
	}

	@Override
	public long getItemId(int position) {
		return (long) contacts.get(position).getId();
	}

	@Override
	public boolean hasStableIds() {
		return false;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		View row = convertView;
		ContactHolder ch = null;

		if (convertView == null) {
			row = inflater.inflate(R.layout.list_view_contacts, parent, false);
			ch = new ContactHolder();
			ch.iv = (ImageView) row.findViewById(R.id.contact_image_view);
			ch.tvName = (TextView) row.findViewById(R.id.contact_tv_name);
			ch.tvTel = (TextView) row.findViewById(R.id.contact_tv_phone_number);
			ch.tvMsg = (TextView) row.findViewById(R.id.contact_tv_message_attached);
			ch.cb = (CheckBox) row.findViewById(R.id.contact_cb_checkbox);

			row.setTag(ch);
		} else {
			ch = (ContactHolder) row.getTag();
		}

		Contact c = contacts.get(position);
		ch.iv.setImageResource(R.drawable.contacts_icon);			//default icon
		ch.tvName.setText(c.getName());
		ch.tvTel.setText(c.getTel());
		if (c.getId_msg() == 0) {
			if (c.getId_grp() != 0) {
				for (int i=0; i<groups.size(); i++) {
					if (c.getId_grp() == groups.get(i).getId()) {
						if (groups.get(i).getId_msg() == 0) {
							ch.tvMsg.setText("");
						} else {
							for (int j=0; j<messages.size(); j++) {
								if (groups.get(i).getId_msg() == messages.get(j).getId()) {
									ch.tvMsg.setText(String.valueOf(messages.get(j).getTitle()));
								}
							}
						}
					}
				}
			} else {
				ch.tvMsg.setText("");
			}
		} else {
			for (int i=0; i<messages.size(); i++) {
				if (c.getId_msg() == messages.get(i).getId()) {
					ch.tvMsg.setText(messages.get(i).getTitle());
				}
			}
		}
		ch.cb.setChecked(checkBoxState[position]);
		ch.cb.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if(((CheckBox)v).isChecked())
					checkBoxState[position]=true;
				else
					checkBoxState[position]=false;
			}
		});

		return row;
	}

	@Override
	public int getItemViewType(int position) {
		return 0;
	}

	@Override
	public int getViewTypeCount() {
		return 1;
	}

	@Override
	public boolean isEmpty() {
		return contacts.size() == 0;
	}

	public boolean[] obtainSelected() {
		String log = "";
		for (int i=0; i<checkBoxState.length; i++) {
			log += "checkBoxState["+i+"]="+checkBoxState[i]+" ; ";
		}
		Log.d("obtainSelected ContactsListViewAdapter", log);
		return checkBoxState;
	}

	static class ContactHolder {
		ImageView iv;
		TextView tvName;
		TextView tvTel;
		TextView tvMsg;
		CheckBox cb;
	}

}
