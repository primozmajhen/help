package net.app.help.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Message;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import net.app.help.R;

/**
 * Created by Boogaboo on 27.12.2014.
 */
public class AddGroupDialog extends DialogFragment {

//	@Override
//	public void onCreate(Bundle savedInstanceState) {
//		super.onCreate(savedInstanceState);
//	}

//	@Nullable
//	@Override
//	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//		View v = inflater.inflate(R.layout.dialog_add_message, container, false);
//
//	EditText etName = (EditText) v.findViewById(R.id.dialog_add_gro_et_name);
//
//		return super.onCreateView(inflater, container, savedInstanceState);
//	}
/*ALI ALI*/

	public static AddGroupDialog newInstance() {
		AddGroupDialog agd = new AddGroupDialog();
		return agd;
	}

	/**Returns Add Group Dialog!*/
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {

		LayoutInflater inflater = getActivity().getLayoutInflater();
		View v = inflater.inflate(R.layout.dialog_add_group, null);
		final EditText etName = (EditText) v.findViewById(R.id.dialog_add_gro_et_name);

		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setView(v)
				.setTitle(R.string.dialog_add_group_title)
				.setPositiveButton(R.string.button_add_group, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						String name = String.valueOf(etName.getText());
						//create new Group entry in the DB !!!
						Log.d("AddGroupDialog onCreateDialog onClick", "CLICKED! name="+name);
					}
				})
				.setNegativeButton(R.string.button_cancel, null);
//		super.onCreateDialog(savedInstanceState);
		return builder.create();
	}

	//	@Override
//	public Dialog onCreateDialog(Bundle savedInstanceState) {
//
//		LayoutInflater inflater = getActivity().getLayoutInflater();
//		View neverShow = inflater.inflate(R.layout.never_show, null);
//
//		checkbox = (CheckBox) neverShow.findViewById(R.id.checkbox);
//
//		// Use the Builder class for convenient dialog construction
//		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
//		builder.setView(neverShow)
//				.setTitle(R.string.location_settings_title)
//				.setMessage(R.string.location_instructions)
//				.setPositiveButton(R.string.location_settings, new DialogInterface.OnClickListener() {
//					public void onClick(DialogInterface dialog, int id) {
//						if (checkbox.isChecked()) {
//							doNotShowAgain();
//						}
//						Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
//						startActivity(myIntent);
//					}
//				})
//				.setNegativeButton(R.string.location_skip, new DialogInterface.OnClickListener() {
//					public void onClick(DialogInterface dialog, int id) {
//						// User cancelled the dialog
//						if (checkbox.isChecked()) {
//							doNotShowAgain();
//						}
//					}
//				});
//		// Create the AlertDialog object and return it
//		return builder.create();
//	}



}
