package net.app.help.adapters;

import android.content.Context;
import android.database.DataSetObserver;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.RadioButton;
import android.widget.TextView;

import net.app.help.R;
import net.app.help.model.Message;

import java.util.ArrayList;

/**ListView z itemom, ki prikazuje Message title in radio button, s katerim se lahko izbere, ter
 * nato pripne k Contact ali Group s pritiskom na Confirm.
 * Created by Boogaboo on 6.1.2015.
 */
public class AttachMessageListViewAdapter extends BaseAdapter implements ListAdapter {

	private boolean[] selectedMessage;
	private ArrayList<Message> messages;
	private LayoutInflater inflater;

	private MessageHolder mh;

	public AttachMessageListViewAdapter(ArrayList<Message> messages, Context context) {
		this.selectedMessage = new boolean[messages.size()];
		this.messages = messages;
		this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public boolean areAllItemsEnabled() {
		return false;
	}

	@Override
	public boolean isEnabled(int position) {
		return false;
	}

	@Override
	public void registerDataSetObserver(DataSetObserver observer) {

	}

	@Override
	public void unregisterDataSetObserver(DataSetObserver observer) {

	}

	@Override
	public int getCount() {
		return messages.size();
	}

	@Override
	public Object getItem(int position) {
		return messages.get(position);
	}

	@Override
	public long getItemId(int position) {
		return (long) messages.get(position).getId();
	}

	@Override
	public boolean hasStableIds() {
		return false;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		View row = convertView;

		if (convertView == null) {
			row = inflater.inflate(R.layout.list_view_att_msg, parent, false);
			mh = new MessageHolder();
			mh.title = (TextView) row.findViewById(R.id.msg_att_tv_title);
			mh.content = (TextView) row.findViewById(R.id.msg_att_tv_content);
			mh.rb = (RadioButton) row.findViewById(R.id.msg_att_radiobutton);
			row.setTag(mh);
		} else {
			mh = (MessageHolder) row.getTag();
		}

		Message m = messages.get(position);
		mh.title.setText(m.getTitle());
		mh.content.setText(m.getContent());
		mh.rb.setChecked(selectedMessage[position]);

		mh.rb.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (((RadioButton)v).isChecked()) {
					selectedMessage[position] = true;
				} else {
					selectedMessage[position] = false;
				}
			}
		});

		return row;
	}

	@Override
	public int getItemViewType(int position) {
		return 0;
	}

	@Override
	public int getViewTypeCount() {
		return 1;
	}

	@Override
	public boolean isEmpty() {
		return false;
	}

	public boolean[] obtainSelected() {
		return selectedMessage;
	}

	private class MessageHolder {
		TextView title;
		TextView content;
		RadioButton rb;
	}

}
