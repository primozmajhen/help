package net.app.help.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import net.app.help.R;
import net.app.help.db.DatabaseAdapter;
import net.app.help.model.Message;

/**
 * Created by Boogaboo on 27.12.2014.
 */
public class AddMessageDialog extends DialogFragment {

//	private Context context;
	private DatabaseAdapter da;
	private TextView tvTitle, tvContent;
	private EditText etTitle, etContent;


	@NonNull
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

		builder.setMessage(R.string.dialog_add_message_message);
		builder.setTitle(R.string.dialog_add_message_title);
		builder.setPositiveButton(R.string.button_add_message, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				//DODAJ NOV MESSAGE V BAZO
				Message m = new Message();
				m.setTitle("");
				m.setContent("");
				da = new DatabaseAdapter(getActivity().getApplicationContext());
				da.openToWrite();
				da.addMessage(m);
				da.close();
				Log.d("DialogFragment onCreateDialog onClick", "setPositiveButton - Message m successfully saved to Database.");
				dismiss();
			}
		});
		builder.setNegativeButton(R.string.button_cancel, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {

				Log.d("DialogFragment onCreateDialog onClick", "setNegativeButton - dismissed.");
				dismiss();
			}
		});
		return builder.create();	//super.onCreateDialog(savedInstanceState);
	}
}
