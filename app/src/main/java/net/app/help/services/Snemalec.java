package net.app.help.services;

import android.app.IntentService;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaRecorder;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

import net.app.help.R;

import java.io.File;
import java.io.IOException;
import java.util.Calendar;


/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p/>
 * TODO: Customize class - update intent actions and extra parameters.
 */
public class Snemalec extends IntentService {

	MediaRecorder mediaRecorder;

//	// TODO: Rename actions, choose action names that describe tasks that this
//	// IntentService can perform, e.g. ACTION_FETCH_NEW_ITEMS
//	public static final String ACTION_FOO = "net.app.help.services.action.FOO";
//	public static final String ACTION_BAZ = "net.app.help.services.action.BAZ";
//
	public static final String ACTION_RECORD = "net.app.help.services.action.REC";
	public static final String ACTION_SAVE = "net.app.help.services.action.SAV";
//	// TODO: Rename parameters
//	public static final String EXTRA_PARAM1 = "net.app.help.services.extra.PARAM1";
//	public static final String EXTRA_PARAM2 = "net.app.help.services.extra.PARAM2";
	public static final String EXTRA_REC_COUNTER = "net.app.help.services.extra.RECCOUNTER";

	public Snemalec() {
		super("Snemalec");
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		if (intent != null) {
//			final String action = intent.getAction();
//			if (ACTION_FOO.equals(action)) {
//				final String param1 = intent.getStringExtra(EXTRA_PARAM1);
//				final String param2 = intent.getStringExtra(EXTRA_PARAM2);
//				handleActionFoo(param1, param2);
//			} else if (ACTION_BAZ.equals(action)) {
//				final String param1 = intent.getStringExtra(EXTRA_PARAM1);
//				final String param2 = intent.getStringExtra(EXTRA_PARAM2);
//				handleActionBaz(param1, param2);
//			}

			final String action = intent.getAction();
			if (ACTION_RECORD.equals(action)) {
				final String param = intent.getStringExtra(EXTRA_REC_COUNTER);
				startRecording(param);
			} else if (ACTION_SAVE.equals(action)) {
				final String param = intent.getStringExtra(EXTRA_REC_COUNTER);
				saveRecording(param);
			}

		}
	}

	private void startRecording(String counter) {
		Calendar c = Calendar.getInstance();
		int month = c.get(Calendar.MONTH) + 1;
		int day = c.get(Calendar.DAY_OF_MONTH);
		int hour = c.get(Calendar.HOUR);
		int min = c.get(Calendar.MINUTE);
		int sec = c.get(Calendar.SECOND);
		String fileName = "HELP_Recording_" + counter + "_Date_" + month + "_" + day + "_Time_" + hour + "-" + min + "-" + sec + ".amr";
		String path = Environment.getExternalStorageDirectory().getAbsolutePath();
		File dir = new File(path);
		if(!dir.exists())
			dir.mkdirs();
		Log.e("recordSound", "FILE PATH: " + path + "/" + fileName);
		mediaRecorder = new MediaRecorder();
		mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
		mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.AMR_WB);
		mediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_WB);
		mediaRecorder.setOutputFile(path + "/" + fileName);
		try {
			mediaRecorder.prepare();
			mediaRecorder.start();
//			Toast.makeText(MainActivity.this, "Recording started", Toast.LENGTH_SHORT).show();
		} catch (IOException e) {
			Log.e("recordSound prepare()", "IOException");
			e.printStackTrace();
			mediaRecorder.stop();
			mediaRecorder.release();
//			Toast.makeText(MainActivity.this, "Recording failed", Toast.LENGTH_LONG).show();
		}//test git
	}

	private void saveRecording(String counter) {
		mediaRecorder.stop();
		mediaRecorder.release();
		mediaRecorder = null;
//		Toast.makeText(MainActivity.this, "Recording stopped. HELP_Recording_"+counter+" with timestamp saved on your device.", Toast.LENGTH_SHORT).show();
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
		SharedPreferences.Editor editor = prefs.edit();
		editor.putInt(getString(R.string.key_recording), Integer.parseInt(counter) + 1);
		editor.apply();
		Log.d("stopRecordingSound","file saved successfully!");
	}

//	/**
//	 * Handle action Foo in the provided background thread with the provided
//	 * parameters.
//	 */
//	private void handleActionFoo(String param1, String param2) {
//		// TODO: Handle action Foo
//		throw new UnsupportedOperationException("Not yet implemented");
//	}
//
//	/**
//	 * Handle action Baz in the provided background thread with the provided
//	 * parameters.
//	 */
//	private void handleActionBaz(String param1, String param2) {
//		// TODO: Handle action Baz
//		throw new UnsupportedOperationException("Not yet implemented");
//	}
}
