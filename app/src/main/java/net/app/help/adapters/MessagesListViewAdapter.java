package net.app.help.adapters;

import android.app.Activity;
import android.app.FragmentManager;
import android.content.Context;
import android.database.DataSetObserver;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ListAdapter;
import android.widget.TextView;

import net.app.help.R;
import net.app.help.db.DatabaseAdapter;
import net.app.help.dialogs.EditMessageDialog;
import net.app.help.model.Message;

import java.util.ArrayList;

/**ListView, ki prikazuje title in (začetek) contenta Message in CheckBox za izbiro, da se jih lahko
 * več izbriše hkrati.
 * Created by Boogaboo on 2.1.2015.
 */
public class MessagesListViewAdapter extends BaseAdapter implements ListAdapter {

	private boolean[] selected;
	private Context context;
	private ArrayList<Message> messages;
	private LayoutInflater inflater;
	private MessageHolder mh;
	private DatabaseAdapter da;
	private FragmentManager fm;

	private Message m;

	public MessagesListViewAdapter(Context context, ArrayList<Message> messages, DatabaseAdapter da) {
		this.selected = new boolean[messages.size()];
		this.context = context;
		this.messages = messages;
		this.da = da;
		inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.fm = ((Activity)context).getFragmentManager();
	}


	@Override
	public boolean areAllItemsEnabled() {
		return true;
	}

	@Override
	public boolean isEnabled(int position) {
		return true;
	}

	@Override
	public void registerDataSetObserver(DataSetObserver observer) {

	}

	@Override
	public void unregisterDataSetObserver(DataSetObserver observer) {

	}

	@Override		//pomembno, najprej se vpraša koliko itemov vsebuje
	public int getCount() {
		return messages.size();
	}

	@Override
	public Object getItem(int position) {
		return messages.get(position);
	}

	@Override
	public long getItemId(int position) {
		return messages.get(position).getId();
	}

	@Override
	public boolean hasStableIds() {
		return true;
	}

	/**Potrebno določiti izgled list itema, vnos vrednosti message v list item*/
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		View row = convertView;
		mh = null;
		if (row == null) {		//if it's not recycled, initialize new view
			row = inflater.inflate(R.layout.list_view_messages, parent, false);
			mh = new MessageHolder();
			mh.titleTV = (TextView) row.findViewById(R.id.message_tv_title);
			mh.contentTV = (TextView) row.findViewById(R.id.message_tv_content);
			mh.checkboxCB = (CheckBox) row.findViewById(R.id.message_checkbox);
			row.setTag(mh);
			//this area is for properties that are all the same
		} else {						//use the recycled view (convert this view)
			mh = (MessageHolder) row.getTag();
		}
		//this area is for properties that are different for each view
		//customize the view instance as appropriate


		mh.titleTV.setText(messages.get(position).getTitle());
		mh.contentTV.setText(messages.get(position).getContent());
		mh.checkboxCB.setChecked(selected[position]);
		mh.checkboxCB.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (((CheckBox)v).isChecked())
					selected[position] = true;
				else
					selected[position] = false;
			}
		});

		row.setOnLongClickListener(new View.OnLongClickListener() {
			@Override
			public boolean onLongClick(View v) {
				Log.d("onLongClick", "position=" + position);
//				OPEN EditMessageDialog in pošlji not izbrano sporočilo
				m = (Message) getItem(position);

				EditMessageDialog emd = EditMessageDialog.newInstance(m);
				emd.show(fm, "EditMessageDialog");

//				AlertDialog.Builder builder = new AlertDialog.Builder(context);
//				builder.setTitle(String.valueOf("Edit message"));
//				View editGroupLayout = inflater.inflate(R.layout.fragment_edit_message, null);
//				title = (EditText) editGroupLayout.findViewById(R.id.frag_edit_msg_et_edit_title);
//				title.setText(String.valueOf(m.getTitle()));
//				content = (EditText) editGroupLayout.findViewById(R.id.frag_edit_msg_et_content);
//				content.setText(String.valueOf(m.getContent()));
//				builder.setView(editGroupLayout);
//				builder.setPositiveButton(R.string.button_confirm, new DialogInterface.OnClickListener() {
//					@Override
//					public void onClick(DialogInterface dialog, int which) {
//						if (!title.getText().toString().equals(m.getTitle()) || !content.getText().toString().equals(m.getContent())) {
//							if (!title.getText().toString().equals(m.getTitle())) {
//								m.setTitle(title.getText().toString());
//								da.openToWrite();
//								da.updateMessage(m);
//								da.close();
//							}
//							if (!content.getText().toString().equals(m.getContent())) {
//								m.setContent(content.getText().toString());
//								da.openToWrite();
//								da.updateMessage(m);
//								da.close();
//							}
//							Toast.makeText(context, "Message edited", Toast.LENGTH_SHORT).show();
////							notifyDataSetChanged();
//							notifyDataSetInvalidated();
//						}
//					}
//				});
//				builder.setNegativeButton(R.string.button_cancel, new DialogInterface.OnClickListener() {
//					@Override
//					public void onClick(DialogInterface dialog, int which) {
//						Toast.makeText(context, "Message edit cancelled", Toast.LENGTH_SHORT).show();
//					}
//				});
//				builder.create();
//				builder.show();
				return false;
			}
		});

		return row;
	}

	@Override
	public int getItemViewType(int position) {
		return 0;
	}

	@Override
	public int getViewTypeCount() {
		return 1;
	}

	@Override
	public boolean isEmpty() {
		return messages.isEmpty();
	}

	public boolean[] obtainSelected() {
		return selected;
	}

	private class MessageHolder {
		TextView titleTV, contentTV;
		CheckBox checkboxCB;
	}

}
