package net.app.help.db;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by Boogaboo on 18.12.2014.
 */
public class DatabaseHelper extends SQLiteOpenHelper {

	public DatabaseHelper(Context context, String dbName, SQLiteDatabase.CursorFactory factory, int dbVersion) {
		super(context, dbName, factory, dbVersion);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		try {
			Log.i("db CREATE DATABASE "+DatabaseAdapter.DB_NAME,
					DatabaseAdapter.SCRIPT_CREATE_TABLE_CONTACTS + "\n" +
					DatabaseAdapter.SCRIPT_CREATE_TABLE_GROUPS + "\n" +
					DatabaseAdapter.SCRIPT_CREATE_TABLE_MESSAGES);
			db.execSQL(DatabaseAdapter.SCRIPT_CREATE_TABLE_CONTACTS);
			db.execSQL(DatabaseAdapter.SCRIPT_CREATE_TABLE_GROUPS);
			db.execSQL(DatabaseAdapter.SCRIPT_CREATE_TABLE_MESSAGES);
		} catch (SQLException sqle) {
			Log.e("DatabaseHelper onCreate ", "Error creating table " + sqle.getMessage());
		}
	}

	/*POTREBNO ZAGOTOVITI STAVEK ZA PREHOD IZ VSAKE NA VSAKO? VERZIJO, ker uporabniku ni potrebno updejtati
	*na zadnjo verzijo ampak kateri vmes... se zakomplicira... več ALTER TABLE...
	*
	* */
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		//DROP OLDER TABLE IF EXISTED
		Log.i("onUpgrade verzija:"+DatabaseAdapter.DB_VERSION,
				DatabaseAdapter.SCRIPT_DROP_TABLE_CONTACTS + "\n" +
						DatabaseAdapter.SCRIPT_DROP_TABLE_GROUPS + "\n" +
						DatabaseAdapter.SCRIPT_DROP_TABLE_MESSAGES);
		db.execSQL(DatabaseAdapter.SCRIPT_DROP_TABLE_CONTACTS);
		db.execSQL(DatabaseAdapter.SCRIPT_DROP_TABLE_GROUPS);
		db.execSQL(DatabaseAdapter.SCRIPT_DROP_TABLE_MESSAGES);
		//CREATE FRESH TABLE
		this.onCreate(db);
	}

}