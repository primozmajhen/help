package net.app.help.model;

import java.util.ArrayList;

/**
 * Created by Boogaboo on 18.12.2014.
 */
public class Group {

	private int id;
	private int id_msg;
	private String name;
	private ArrayList<Contact> contacts;		/*NAJVERJETNEJE ODVEČ, SAJ JE KONCEPT POSTAVLJEN NA GLAVO
	NAMREČ, ZA UGOTOVITEV KATERI KONTAKTI PRIPADAJO SKUPINI JE POTREBNO NAJPREJ PREBRATI SEZNAM KONTAKTOV,
	NATO PREBRATI SEZNAM SKUPIN IN NATO PO group.id in contact.grp_id UGOTOVITI KATERE KONTAKTE SKUPINA VSEBUJE.
	IZ TEGA RAZLOGA JE ArrayList<Contact> contacts odveč, saj se ne uporablja v bazi in se v bazo tako tudi
	ne shranjuje. Lahko se uporabi le, če ta logika pride prav pri samem programiranju, a mislim, da se je
	temu načinu, ki se ne sklada z bazo najboljše izogniti in ga nadomestiti z dodatnimi povpraševanji iz baze.*/

	@Override
	public String toString() {
		return "ID="+id+" ID_MSG="+id_msg+" NAME="+name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getId_msg() {
		return id_msg;
	}

	public void setId_msg(int id_msg) {
		this.id_msg = id_msg;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ArrayList<Contact> getContacts() {
		return contacts;
	}

	public void setContacts(ArrayList<Contact> contacts) {
		this.contacts = contacts;
	}

	public void addContact(Contact contact) {
		this.contacts.add(contact);
	}

	public void removeContact(Contact contact) {
		this.contacts.remove(contact);
	}

	public void removeContact(int index) {
		this.contacts.remove(index);
	}
}
