package net.app.help.model;

/**
 * Created by Boogaboo on 18.12.2014.
 */
public class Message {

	private int id;
	private String title;
	private String content;

	public Message() {

	}

	@Override
	public String toString() {
		return "ID="+ this.id + " TITLE=" + this.title + " CONTENT=" + this.content;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
}
